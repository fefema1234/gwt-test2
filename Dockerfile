FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/rest-service-complete.war
COPY ${JAR_FILE} app.war
ENTRYPOINT ["java","-jar","/app.war"]