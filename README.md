# Spring Boot GWT

Test Spring Boot GWT + PostgeSQL

## Build

```
./deploy.sh
```

## Run

Run docker-compose:

```
docker-compose up -d
```

Open in browser:

```
http://localhost:8080/
```

In the system there is only one customer with Id: 1

Thank You!