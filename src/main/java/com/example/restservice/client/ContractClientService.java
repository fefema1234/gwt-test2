package com.example.restservice.client;

import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.RestService;

import javax.ws.rs.*;
import java.util.List;

@Path("/api/v1/insurance-contract")
public interface ContractClientService extends RestService {

    @GET
    void findAll(MethodCallback<List<ContractItem>> callback);

    @POST
    void create(final ContractItem contractItem, MethodCallback<Void> callback);

    @POST
    @Path("/delete/{id}")
    void delete(@PathParam("id") Long id, MethodCallback<Void> callback);
}
