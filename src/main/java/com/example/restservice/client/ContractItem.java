package com.example.restservice.client;

class ContractItem {

    private Long id;

    private String createDate;

    private String contractNumber;

    private String startDate;

    private String endDate;

    private Long customerId;

    private Double insuranceSum;

    private Double contractSum;

    private String status;

    public ContractItem() {

    }

    public ContractItem(Long customerId, String contractNumber, Double insuranceSum, Double contractSum) {
        this.contractNumber = contractNumber;
        this.customerId = customerId;
        this.insuranceSum = insuranceSum;
        this.contractSum = contractSum;
        this.createDate = "2022-05-12T16:30:00Z";
        this.startDate = "2022-05-12T16:30:00Z";
        this.endDate = "2022-05-12T16:30:00Z";
        this.status = "ACTIVE";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Double getInsuranceSum() {
        return insuranceSum;
    }

    public void setInsuranceSum(Double insuranceSum) {
        this.insuranceSum = insuranceSum;
    }

    public Double getContractSum() {
        return contractSum;
    }

    public void setContractSum(Double contractSum) {
        this.contractSum = contractSum;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " " + customerId + " " + contractNumber + " " + insuranceSum + " " + contractSum + " " + status;
    }
}
