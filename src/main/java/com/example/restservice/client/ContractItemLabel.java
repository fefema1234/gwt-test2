package com.example.restservice.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;

import java.util.ArrayList;
import java.util.List;

class ContractItemLabel extends Composite {

    interface TodoItemLabelUiBinder extends UiBinder<Label, ContractItemLabel> {
    }

    private static TodoItemLabelUiBinder ourUiBinder = GWT.create(TodoItemLabelUiBinder.class);

    public interface TodoItemLabelClickHandler {
        void onClick(final ContractItem contractItem);
    }

    private final List<TodoItemLabelClickHandler> clickHandlers;

    @UiField
    Label label;

    public ContractItemLabel(final ContractItem contractItem) {
        initWidget(ourUiBinder.createAndBindUi(this));
        label.setText(contractItem.toString());
        clickHandlers = new ArrayList<>();

        addDomHandler(event -> {
            for (final TodoItemLabelClickHandler clickHandler : clickHandlers) {
                clickHandler.onClick(contractItem);
            }
        }, ClickEvent.getType());
    }

    public void addClickHandler(final TodoItemLabelClickHandler clickHandler) {
        clickHandlers.add(clickHandler);
    }
}