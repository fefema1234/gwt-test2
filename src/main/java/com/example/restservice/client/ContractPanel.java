package com.example.restservice.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;

import java.util.List;

class ContractPanel extends Composite {

    interface TestViewUiBinder extends UiBinder<HTMLPanel, ContractPanel> {
    }

    private static TestViewUiBinder ourUiBinder = GWT.create(TestViewUiBinder.class);

    private static final ContractClientService contractClientService = GWT.create(ContractClientService.class);

    @UiField
    FlowPanel contractItemsList;

    @UiField
    TextBox contractCustomerIdTextBox;

    @UiField
    TextBox contractNumberTextBox;

    @UiField
    TextBox contractInsuranceSumTextBox;

    @UiField
    TextBox contractContractSumTextBox;

    @UiField
    Button addContractItemButton;

    public ContractPanel() {
        initWidget(ourUiBinder.createAndBindUi(this));
        refreshContractItems();

        addContractItemButton.addClickHandler(event -> {
            addValidateContractItem();
        });

        initCreateContractTextBo();

        contractCustomerIdTextBox.addKeyUpHandler(event -> {
            if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                addValidateContractItem();
            }
        });
    }

    private void refreshContractItems() {
        contractClientService.findAll(new MethodCallback<List<ContractItem>>() {
            @Override
            public void onFailure(final Method method, final Throwable exception) {

            }

            @Override
            public void onSuccess(final Method method, final List<ContractItem> response) {
                contractItemsList.clear();
                for (final ContractItem contractItem : response) {
                    final ContractItemLabel contractItemItemLabel = new ContractItemLabel(contractItem);
                    contractItemItemLabel.addClickHandler(contractItemToRemove -> deleteContractItem(contractItemToRemove));
                    contractItemsList.add(contractItemItemLabel);
                }
            }
        });
    }

    private void addContractItem(final ContractItem contractItem) {
        contractClientService.create(contractItem, new MethodCallback<Void>() {
            @Override
            public void onFailure(final Method method, final Throwable exception) {

            }

            @Override
            public void onSuccess(final Method method, final Void response) {
                clearCreateContractTextBox();
                refreshContractItems();
            }
        });
    }

    public void deleteContractItem(final ContractItem contractItem) {
        contractClientService.delete(contractItem.getId(), new MethodCallback<Void>() {
            @Override
            public void onFailure(final Method method, final Throwable exception) {

            }

            @Override
            public void onSuccess(final Method method, final Void response) {
                refreshContractItems();
            }
        });
    }

    private void initCreateContractTextBo() {
        contractCustomerIdTextBox.getElement().setAttribute("placeholder", "customerId");
        contractNumberTextBox.getElement().setAttribute("placeholder", "contractNumber");
        contractInsuranceSumTextBox.getElement().setAttribute("placeholder", "insuranceSum");
        contractContractSumTextBox.getElement().setAttribute("placeholder", "contractSum");
    }

    private void clearCreateContractTextBox() {
        contractCustomerIdTextBox.setText("");
        contractNumberTextBox.setText("");
        contractInsuranceSumTextBox.setText("");
        contractContractSumTextBox.setText("");
    }

    private void addValidateContractItem() {
        final Long customerId = Long.parseLong(contractCustomerIdTextBox.getText());
        final String contractNumber = contractNumberTextBox.getText();
        final Double insuranceSum = Double.parseDouble(contractInsuranceSumTextBox.getText());
        final Double contractSum = Double.parseDouble(contractContractSumTextBox.getText());

        final ContractItem contractItem = new ContractItem(customerId, contractNumber, insuranceSum, contractSum);

        if (!contractItem.getContractNumber().isEmpty()) {
            addContractItem(contractItem);
        } else {
            Window.alert("Incorrect data!");
        }
    }
}