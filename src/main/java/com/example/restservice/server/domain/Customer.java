package com.example.restservice.server.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "customer")
public class Customer implements Serializable {

    private static final long serialVersionUID = -7526902496230653275L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "last_name", length = 50)
    private String lastName;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "middle_name", length = 50)
    private String middleName;

    @NotEmpty
    @Column(name = "birth_date")
    private Instant birthDate;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "phone", length = 50)
    private String phone;
}
