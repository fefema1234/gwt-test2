package com.example.restservice.server.domain;

import com.example.restservice.server.domain.enumerations.ContractStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;

import static com.example.restservice.server.domain.enumerations.ContractStatus.ACTIVE;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "insurance_contract")
public class InsuranceContract implements Serializable {

    private static final long serialVersionUID = -3724842464732104427L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotEmpty
    @Column(name = "create_date")
    private Instant createDate;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "contract_number", length = 50, unique = true)
    private String contractNumber;

    @NotEmpty
    @Column(name = "start_date")
    private Instant startDate;

    @NotEmpty
    @Column(name = "end_date")
    private Instant endDate;

    @Builder.Default
    @NotEmpty
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ContractStatus status = ACTIVE;

    @NotEmpty
    @ManyToOne
    private Customer customer;

    @NotEmpty
    @Column(name = "insurance_sum")
    private Double insuranceSum;

    @NotEmpty
    @Column(name = "contract_sum")
    private Double contractSum;
}
