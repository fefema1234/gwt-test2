package com.example.restservice.server.domain.enumerations;

public enum ContractStatus {
    ACTIVE,
    DELETED
}
