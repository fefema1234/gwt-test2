package com.example.restservice.server.repository;

import com.example.restservice.server.domain.InsuranceContract;
import com.example.restservice.server.domain.enumerations.ContractStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InsuranceContractRepository extends JpaRepository<InsuranceContract, Long> {

    @Transactional
    @Modifying(flushAutomatically = true, clearAutomatically = true)
    @Query("update InsuranceContract c set c.status = :status where c.id = :id")
    void updateStatus(@Param("id") Long id, @Param("status") ContractStatus status);
}
