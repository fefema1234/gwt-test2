package com.example.restservice.server.service;

import com.example.restservice.server.service.dto.CustomerDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CustomerService {

    CustomerDTO save(CustomerDTO customerDTO);

    List<CustomerDTO> findAll(Pageable pageable);

    Optional<CustomerDTO> findOne(Long id);
}
