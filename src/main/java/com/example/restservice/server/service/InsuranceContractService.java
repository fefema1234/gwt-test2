package com.example.restservice.server.service;

import com.example.restservice.server.service.dto.InsuranceContractDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface InsuranceContractService {

    InsuranceContractDTO save(InsuranceContractDTO insuranceContractDTO);

    void delete(Long id);

    List<InsuranceContractDTO> findAll(Pageable pageable);

    Optional<InsuranceContractDTO> findOne(Long id);
}
