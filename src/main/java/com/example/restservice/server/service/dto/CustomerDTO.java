package com.example.restservice.server.service.dto;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerDTO implements Serializable {

    private static final long serialVersionUID = 1694252008773858585L;

    private Long id;

    @NotEmpty
    @Size(max = 50)
    private String firstName;

    @NotEmpty
    @Size(max = 50)
    private String lastName;

    @NotEmpty
    @Size(max = 50)
    private String middleName;

    @NotEmpty
    private Instant birthDate;

    @NotEmpty
    @Size(max = 50)
    private String phone;
}
