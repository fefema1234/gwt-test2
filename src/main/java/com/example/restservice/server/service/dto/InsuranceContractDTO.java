package com.example.restservice.server.service.dto;

import com.example.restservice.server.domain.enumerations.ContractStatus;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.Instant;

import static com.example.restservice.server.domain.enumerations.ContractStatus.ACTIVE;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InsuranceContractDTO implements Serializable {

    private static final long serialVersionUID = -460440658694669514L;

    private Long id;

    @NotEmpty
    private Instant createDate;

    @NotEmpty
    private String contractNumber;

    @NotEmpty
    private Instant startDate;

    @NotEmpty
    private Instant endDate;

    @NotEmpty
    private Long customerId;

    @NotEmpty
    private Double insuranceSum;

    @NotEmpty
    private Double contractSum;

    @Builder.Default
    @NotEmpty
    private ContractStatus status = ACTIVE;
}
