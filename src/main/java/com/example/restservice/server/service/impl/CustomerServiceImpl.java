package com.example.restservice.server.service.impl;

import com.example.restservice.server.repository.CustomerRepository;
import com.example.restservice.server.service.CustomerService;
import com.example.restservice.server.service.dto.CustomerDTO;
import com.example.restservice.server.service.mapper.CustomerMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerMapper customerMapper;

    @Override
    public CustomerDTO save(final CustomerDTO customerDTO) {
        final var createdCustomer = customerRepository.save(customerMapper.toEntity(customerDTO));
        return customerMapper.toDto(createdCustomer);
    }

    @Override
    public List<CustomerDTO> findAll(final Pageable pageable) {
        return customerMapper.toDto(customerRepository.findAll(pageable).getContent());
    }

    @Override
    public Optional<CustomerDTO> findOne(final Long id) {
        return Optional.of(customerMapper.toDto(customerRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Invalid customer id"))));
    }
}
