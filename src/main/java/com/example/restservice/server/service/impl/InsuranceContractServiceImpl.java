package com.example.restservice.server.service.impl;

import com.example.restservice.server.repository.InsuranceContractRepository;
import com.example.restservice.server.service.InsuranceContractService;
import com.example.restservice.server.service.dto.InsuranceContractDTO;
import com.example.restservice.server.service.mapper.InsuranceContractMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.restservice.server.domain.enumerations.ContractStatus.DELETED;

@Slf4j
@Service
@AllArgsConstructor
public class InsuranceContractServiceImpl implements InsuranceContractService {

    private final InsuranceContractRepository insuranceContractRepository;

    private final InsuranceContractMapper insuranceContractMapper;

    @Override
    public InsuranceContractDTO save(final InsuranceContractDTO insuranceContractDTO) {
        final var insuranceContract = insuranceContractRepository.save(insuranceContractMapper.toEntity(insuranceContractDTO));
        return insuranceContractMapper.toDto(insuranceContract);
    }

    @Override
    public void delete(final Long id) {
        insuranceContractRepository.updateStatus(id, DELETED);
    }

    @Override
    public List<InsuranceContractDTO> findAll(final Pageable pageable) {
        return insuranceContractMapper.toDto(insuranceContractRepository.findAll(pageable).getContent());
    }

    @Override
    public Optional<InsuranceContractDTO> findOne(final Long id) {
        return Optional.of(insuranceContractMapper.toDto(insuranceContractRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Invalid contract id"))));
    }
}
