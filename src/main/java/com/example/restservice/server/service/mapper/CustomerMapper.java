package com.example.restservice.server.service.mapper;

import com.example.restservice.server.domain.Customer;
import com.example.restservice.server.service.dto.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    default Customer fromId(Long id) {
        return id != null ? Customer.builder().id(id).build() : null;
    }
}
