package com.example.restservice.server.service.mapper;

import com.example.restservice.server.domain.InsuranceContract;
import com.example.restservice.server.service.dto.InsuranceContractDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CustomerMapper.class})
public interface InsuranceContractMapper extends EntityMapper<InsuranceContractDTO, InsuranceContract> {

    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "insuranceSum", target = "insuranceSum", numberFormat = "$#.00")
    @Mapping(source = "contractSum", target = "contractSum", numberFormat = "$#.00")
    InsuranceContractDTO toDto(InsuranceContract insuranceContract);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "insuranceSum", target = "insuranceSum", numberFormat = "$#.00")
    @Mapping(source = "contractSum", target = "contractSum", numberFormat = "$#.00")
    InsuranceContract toEntity(InsuranceContractDTO insuranceContractDTO);

    default InsuranceContract fromId(Long id) {
        return id != null ? InsuranceContract.builder().id(id).build() : null;
    }
}
