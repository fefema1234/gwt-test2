package com.example.restservice.server.web.rest;

import com.example.restservice.server.service.CustomerService;
import com.example.restservice.server.service.dto.CustomerDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.example.restservice.server.web.rest.ResponseUtil.wrapOrNotFound;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/customer")
public class CustomerResource {

    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<CustomerDTO> create(final @Valid @RequestBody CustomerDTO customerDTO) {
        log.debug("REST request to create customer : {}", customerDTO);
        final var createdCustomerDTO = customerService.save(customerDTO);
        return ResponseEntity.ok().body(createdCustomerDTO);
    }

    @GetMapping
    public ResponseEntity<List<CustomerDTO>> findAll(final Pageable pageable) {
        log.debug("REST request to find all customers : {}", pageable);
        final var customerDTOS = customerService.findAll(pageable);
        return ResponseEntity.ok().body(customerDTOS);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> findOne(final @PathVariable Long id) {
        log.debug("REST request to find customer : {}", id);
        final var customerDTO = customerService.findOne(id);
        return wrapOrNotFound(customerDTO);
    }
}
