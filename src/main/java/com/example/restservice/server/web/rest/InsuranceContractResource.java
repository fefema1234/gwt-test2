package com.example.restservice.server.web.rest;

import com.example.restservice.server.service.InsuranceContractService;
import com.example.restservice.server.service.dto.InsuranceContractDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.example.restservice.server.web.rest.ResponseUtil.wrapOrNotFound;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/insurance-contract")
public class InsuranceContractResource {

    private final InsuranceContractService insuranceContractService;

    @PostMapping
    public ResponseEntity<InsuranceContractDTO> create(final @Valid @RequestBody InsuranceContractDTO insuranceContractDTO) {
        log.debug("REST request to create InsuranceContract : {}", insuranceContractDTO);
        final var createdInsuranceContractDTO = insuranceContractService.save(insuranceContractDTO);
        return ResponseEntity.ok().body(createdInsuranceContractDTO);
    }

    @PostMapping("/delete/{id}")
    public ResponseEntity<Void> delete(final @PathVariable Long id) {
        log.debug("REST request to delete InsuranceContract : {}", id);
        insuranceContractService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<InsuranceContractDTO>> findAll(final Pageable pageable) {
        log.debug("REST request to find all InsuranceContracts : {}", pageable);
        final var insuranceContractDTOS = insuranceContractService.findAll(pageable);
        return ResponseEntity.ok().body(insuranceContractDTOS);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InsuranceContractDTO> findOne(final @PathVariable Long id) {
        log.debug("REST request to find InsuranceContract : {}", id);
        final var insuranceContractDTO = insuranceContractService.findOne(id);
        return wrapOrNotFound(insuranceContractDTO);
    }
}
