package com.example.restservice.container;

import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;

import java.time.Duration;
import java.util.TimeZone;

import static java.time.temporal.ChronoUnit.SECONDS;

public class PostgreSQLTestContainer extends PostgreSQLContainer<PostgreSQLTestContainer> {
    private static final String IMAGE_VERSION = "postgres:12.2";
    private static volatile PostgreSQLTestContainer container;

    private PostgreSQLTestContainer() {
        super(IMAGE_VERSION);
    }

    public static PostgreSQLTestContainer getInstance(boolean withReuse) {
        if (container == null) {
            synchronized (PostgreSQLTestContainer.class) {
                if (container == null) {
                    container = new PostgreSQLTestContainer().withReuse(withReuse);

                    container.setWaitStrategy(new LogMessageWaitStrategy()
                            .withRegEx(".*database system is ready to accept connections.*\\s")
                            .withTimes(1)
                            .withStartupTimeout(Duration.of(30, SECONDS)));
                }
            }
        }

        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public void stop() {
    }
}
