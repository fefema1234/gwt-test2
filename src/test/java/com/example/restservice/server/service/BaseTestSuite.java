package com.example.restservice.server.service;

import com.example.restservice.container.PostgreSQLTestContainer;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@SpringBootTest
public abstract class BaseTestSuite {

    @Container
    private static PostgreSQLContainer postgreSQLContainer = PostgreSQLTestContainer.getInstance(true);
}
