package com.example.restservice.server.service;

import com.example.restservice.server.service.dto.CustomerDTO;
import junit.framework.AssertionFailedError;
import lombok.var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class CustomerServiceIT extends BaseTestSuite {

    @Autowired
    private CustomerService customerService;

    @Test
    void createCustomer() {
        final var createdCustomerDTO = customerService.save(CustomerDTO.builder()
                .firstName(UUID.randomUUID().toString())
                .lastName(UUID.randomUUID().toString())
                .middleName(UUID.randomUUID().toString())
                .birthDate(Instant.now())
                .phone(UUID.randomUUID().toString())
                .build());

        final var createdCustomer = customerService.findOne(createdCustomerDTO.getId()).orElseThrow(AssertionFailedError::new);
        assertThat(createdCustomer.getFirstName()).isEqualTo(createdCustomerDTO.getFirstName());
        assertThat(createdCustomer.getLastName()).isEqualTo(createdCustomerDTO.getLastName());
        assertThat(createdCustomer.getMiddleName()).isEqualTo(createdCustomerDTO.getMiddleName());
        assertThat(createdCustomer.getPhone()).isEqualTo(createdCustomerDTO.getPhone());
    }
}
