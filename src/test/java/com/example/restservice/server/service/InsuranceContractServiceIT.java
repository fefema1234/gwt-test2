package com.example.restservice.server.service;

import com.example.restservice.server.service.dto.CustomerDTO;
import com.example.restservice.server.service.dto.InsuranceContractDTO;
import junit.framework.AssertionFailedError;
import lombok.var;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

import static com.example.restservice.server.domain.enumerations.ContractStatus.ACTIVE;
import static com.example.restservice.server.domain.enumerations.ContractStatus.DELETED;
import static org.assertj.core.api.Assertions.assertThat;

class InsuranceContractServiceIT extends BaseTestSuite {

    @Autowired
    private InsuranceContractService insuranceContractService;

    @Autowired
    private CustomerService customerService;

    @Test
    void createInsuranceContract() {
        final var createdCustomerDTO = customerService.save(CustomerDTO.builder()
                .firstName(UUID.randomUUID().toString())
                .lastName(UUID.randomUUID().toString())
                .middleName(UUID.randomUUID().toString())
                .birthDate(Instant.now())
                .phone(UUID.randomUUID().toString())
                .build());

        final var createdInsuranceContractDTO = insuranceContractService.save(InsuranceContractDTO.builder()
                .contractNumber(UUID.randomUUID().toString())
                .customerId(createdCustomerDTO.getId())
                .createDate(Instant.now())
                .startDate(Instant.now())
                .endDate(Instant.now())
                .insuranceSum(1000.00)
                .contractSum(5000.55)
                .build());

        final var createdInsuranceContract = insuranceContractService.findOne(createdInsuranceContractDTO.getId()).orElseThrow(AssertionFailedError::new);
        assertThat(createdInsuranceContract.getContractNumber()).isEqualTo(createdInsuranceContractDTO.getContractNumber());
        assertThat(createdInsuranceContract.getInsuranceSum()).isEqualTo(createdInsuranceContractDTO.getInsuranceSum());
        assertThat(createdInsuranceContract.getContractSum()).isEqualTo(createdInsuranceContractDTO.getContractSum());
        assertThat(createdInsuranceContract.getStatus()).isEqualTo(ACTIVE);
    }

    @Test
    void deleteInsuranceContract() {
        final var createdCustomerDTO = customerService.save(CustomerDTO.builder()
                .firstName(UUID.randomUUID().toString())
                .lastName(UUID.randomUUID().toString())
                .middleName(UUID.randomUUID().toString())
                .birthDate(Instant.now())
                .phone(UUID.randomUUID().toString())
                .build());

        final var createdInsuranceContractDTO = insuranceContractService.save(InsuranceContractDTO.builder()
                .contractNumber(UUID.randomUUID().toString())
                .customerId(createdCustomerDTO.getId())
                .createDate(Instant.now())
                .startDate(Instant.now())
                .endDate(Instant.now())
                .insuranceSum(1000.00)
                .contractSum(5000.55)
                .build());

        insuranceContractService.delete(createdInsuranceContractDTO.getId());

        final var createdInsuranceContract = insuranceContractService.findOne(createdInsuranceContractDTO.getId()).orElseThrow(AssertionFailedError::new);
        assertThat(createdInsuranceContract.getContractNumber()).isEqualTo(createdInsuranceContractDTO.getContractNumber());
        assertThat(createdInsuranceContract.getInsuranceSum()).isEqualTo(createdInsuranceContractDTO.getInsuranceSum());
        assertThat(createdInsuranceContract.getContractSum()).isEqualTo(createdInsuranceContractDTO.getContractSum());
        assertThat(createdInsuranceContract.getStatus()).isEqualTo(DELETED);
    }
}
